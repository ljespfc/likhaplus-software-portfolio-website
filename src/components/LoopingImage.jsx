import React from "react";
import styled from "styled-components";

const StyledLoopingImage = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;

  img {
    height: 100%;
    position: absolute;
    animation-name: fade;
    animation-iteration-count: infinite;
    animation-duration: 6s;

    &:nth-of-type(1) {
      animation-delay: 06s;
    }
    &:nth-of-type(2) {
      animation-delay: -2s;
    }
    &:nth-of-type(3) {
      animation-delay: -4s;
    }
  }

  @keyframes fade {
    0% {
      opacity: 0;
    }
    20% {
      opacity: 1;
    }
    33% {
      opacity: 1;
    }
    53% {
      opacity: 0;
    }
    100% {
      opacity: 0;
    }
  }
`;

function LoopingImage({ imageSrcs }) {
  return (
    <StyledLoopingImage>
      {imageSrcs.map(imageSrc => (
        <img src={imageSrc} alt="" />
      ))}
    </StyledLoopingImage>
  );
}

export default LoopingImage;
