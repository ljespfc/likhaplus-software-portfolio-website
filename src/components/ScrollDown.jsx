import React from "react";
import styled from "styled-components";

const StyledScrollDown = styled.div`
  bottom: 40px;
  height: 100px;
  margin-left: -50px;
  position: absolute;
  left: 50%;
  text-align: center;
  width: 100px;
  z-index: 100;
  color: ${p => p.theme.color.primary.main};

  p {
    animation-duration: 2s;
    animation-fill-mode: both;
    animation-iteration-count: infinite;
    animation-name: scroll;

    text-transform: uppercase;
    text-indent: 1px;
  }

  .mouse {
    border: 2px solid ${p => p.theme.color.primary.main};
    border-radius: 13px;
    display: block;
    height: 46px;
    left: 50%;
    margin: 10px 0 0 -13px;
    position: absolute;
    width: 28px;
  }

  span {
    display: block;
    font-size: 1.2em;
    margin: 6px auto;
  }

  @keyframes scroll {
    0% {
      opacity: 1;
      transform: translateY(0px);
    }
    100% {
      opacity: 0;
      transform: translateY(10px);
    }
  }
`;

function ScrollDown() {
  return (
    <StyledScrollDown>
      <p>scroll</p>
      <div className="mouse">
        <span>
          <p>↓</p>
        </span>
      </div>
    </StyledScrollDown>
  );
}

export default ScrollDown;
