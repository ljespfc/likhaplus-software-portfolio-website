import React from "react";
import styled from "styled-components";

import mockup01 from "./mockup-01.png";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .projects-group {
    display: flex;
    justify-content: center;
    height: 100%;

    > *:not(:last-child) {
      margin-right: ${p => p.theme.size.xl};

      @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
        margin-right: 0;
        margin-bottom: ${p => p.theme.size.xl};
      }
    }

    @media (max-width: ${p => p.theme.breakpoint.desktopM}) {
      flex-flow: column;
    }
  }

  .project-item-compound {
    display: inline-block;
    text-align: center;
  }

  .project-image {
    margin-bottom: ${p => p.theme.size.m};
    max-width: 100%;
  }

  .project-title-text {
    ${p => p.theme.typographyMixin.headline2};
    margin-bottom: ${p => p.theme.size.m};
  }

  .project-description-text {
  }
`;

const SlideFour = () => {
  return (
    <StyledSlideSection
      color="primary"
      layout="titleAndContent"
      title="Our Projects"
    >
      <section className="projects-group">
        <article className="project-item-compound">
          <img className="project-image" src={mockup01} alt="" />
          <p className="project-title-text">FliqCliq</p>
          <p className="project-description-text">
            Cupidatat ipsum duis eiusmod ad duis culpa duis culpa do labore
            exercitation ad ad reprehenderit.
          </p>
        </article>

        <article className="project-item-compound">
          <img className="project-image" src={mockup01} alt="" />
          <p className="project-title-text">Work Tayo</p>
          <p className="project-description-text">
            Cupidatat ipsum duis eiusmod ad duis culpa duis culpa do labore
            exercitation ad ad reprehenderit.
          </p>
        </article>
      </section>
    </StyledSlideSection>
  );
};

export default SlideFour;
