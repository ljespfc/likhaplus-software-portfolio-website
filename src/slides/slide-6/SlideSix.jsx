import React from "react";
import styled from "styled-components";

import SlideSection from "../../components/SlideSection";

const StyledSlideSection = styled(SlideSection)`
  .list {
    margin-left: ${p => p.theme.size.m};
    ${p => p.theme.typographyMixin.headline2};

    li:not(:last-child) {
      margin-bottom: ${p => p.theme.size.m};
    }

    @media (max-width: ${p => p.theme.breakpoint.desktopL}) {
      font-size: ${p => p.theme.typography.scale.display3};
    }

    @media (max-width: ${p => p.theme.breakpoint.tabletPortrait}) {
      font-size: ${p => p.theme.typography.scale.display4};
    }
  }
`;

const SlideTen = () => {
  return (
    <StyledSlideSection
      color="secondary"
      layout="titleAndContent"
      title="Services"
      peekColor="primary"
    >
      <ul className="list">
        <li>Website</li>
        <li>E-commerce</li>
        <li>Content Management System (CMS)</li>
        <li>Enterprise Resource Planning (ERP)</li>
        <li>Android App</li>
        <li>iOS App</li>
        <li>Games</li>
        <li>TV App</li>
        <li>UX Research</li>
        <li>UI Design</li>
      </ul>
    </StyledSlideSection>
  );
};

export default SlideTen;
