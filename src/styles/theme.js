import { css } from "styled-components";

const breakpoint = {
  phone: "0em",
  tabletPortrait: "37.5em", // 600px
  tabletLandscape: "56.25em", // 900px
  desktopM: "75em", // 1200px
  desktopL: "93.75em", // 1500px
  desktopXL: "112.5em", // 1800px
  desktopXXL: "125em", // 2000px
};

const typography = {
  primary: "'Roboto', sans-serif",
  scale: {
    small: "0.875em", // 14px
    base: "1em", // 16px
    body: "1.1875em", // 19px
    display4: "1.1875em", // 24px
    display3: "1.5em", // 24px
    display2: "2em", // 32px
    display1: "3.875em", // 64px
  },
};

const typographyMixin = {
  headline1: css`
    font-size: ${typography.scale.display1};
    font-weight: 900;

    @media (max-width: ${breakpoint.tabletPortrait}) {
      font-size: ${typography.scale.display2};
    }
  `,
  headline2: css`
    font-size: ${typography.scale.display2};
    font-weight: 600;

    @media (max-width: ${breakpoint.tabletLandscape}) {
      font-size: ${typography.scale.display3};
    }
  `,
};

export default {
  breakpoint,
  color: {
    primary: {
      main: "#7030A0",
    },
    secondary: {
      main: "#FFC000",
    },
    basic: {
      white: "#ffffff",
    },
    grey: {
      dark: "#404040",
      medium: "#808080",
      light: "#B9B9B9",
    },
  },
  typography,
  typographyMixin,
  size: {
    xxs: "0.125rem", // 2px
    xs: "0.25rem", // 4px
    s: "0.5rem", // 8px
    m: "1rem", // 16px
    l: "2rem", // 32px
    xl: "4rem", // 64px
    base: "1.5rem", // 24px
    base: "3rem", // 48px
    baseHalf: "0.75rem", // 12px
    increment: increment => `${increment}rem`,
  },
  shadow: [
    "0 0.0625em 0.1875em rgba(0,0,0,0.12), 0 0.0625em 0.125em rgba(0,0,0,0.16)",
    "0 0.1875em 0.375em rgba(0,0,0,0.16), 0 0.1875em 0.375em rgba(0,0,0,0.16)",
    "0 0.625em 1.25em rgba(0,0,0,0.19), 0 0.375em 0.375em rgba(0,0,0,0.16)",
    "0 0.875em 1.75em rgba(0,0,0,0.25), 0 0.625em 0.625em rgba(0,0,0,0.16)",
    "0 1.1875em 2.375em rgba(0,0,0,0.30), 0 0.9375em 0.75em rgba(0,0,0,0.16)",
  ],
};
