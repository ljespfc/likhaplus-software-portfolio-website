import { createGlobalStyle } from "styled-components";
import theme from "./theme";

export default createGlobalStyle`
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }

    html,
    body {
        height: 100vh;
        overflow: hidden;
    }

    body {
        font-family: ${theme.typography.primary};
    }

    h1, h2, h3, h4, h5 {
        font-weight: unset;
    }

    .bold {
        font-weight: 600;
    } 

    a {
        color: inherit;
        text-decoration: none;
    }
`;
